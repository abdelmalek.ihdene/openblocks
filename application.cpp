#include <cstdio>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/mat4x4.hpp>
#include <initializer_list>
#include <iostream>
#include <SDL2/SDL.h>
#include <vector>

#ifdef EMSCRIPTEN
#include "emscripten.h"
#include <GLES3/gl3.h>
#else
#include <gl.h>
#endif

#include "include/application.hpp"
#include "include/block_generator.hpp"
#include "include/camera.hpp"
#include "include/shader_manager.hpp"
#include "include/utility.hpp"

Application::Application() {
  initialiseSDL();
  initialiseOpenGL();
  initialiseOpenGLShaders();
  initialiseTextures();
  initialiseScene();
}
Application::~Application() {
  delete chunkMan;
  delete skybox;
  delete camera;
  delete shaderMan;
  delete atlas;
  SDL_GL_DeleteContext(context);
  SDL_DestroyWindow(window);
  SDL_Quit();
}
void Application::handleEvent(SDL_Event &event) {
  if(event.type == SDL_QUIT) {
    running = false;
  } else if(event.type == SDL_KEYDOWN) {
    handleKeyDownEvent(event.key.keysym.scancode);
  } else if(event.type == SDL_MOUSEMOTION) {
    if(SDL_GetRelativeMouseMode() == SDL_TRUE)
      camera->rotate(glm::vec2(event.motion.xrel, event.motion.yrel));
  }
}
void Application::handleKeyboardState(unsigned char const *state) {
  if(SDL_GetRelativeMouseMode() == SDL_TRUE) {
    if(state[SDL_SCANCODE_W]) camera->move(Camera::MOVE_FORWARD);
    if(state[SDL_SCANCODE_A]) camera->move(Camera::MOVE_LEFT);
    if(state[SDL_SCANCODE_S]) camera->move(Camera::MOVE_BACKWARD);
    if(state[SDL_SCANCODE_D]) camera->move(Camera::MOVE_RIGHT);
    if(state[SDL_SCANCODE_SPACE]) camera->move(Camera::JUMP);
  }
}
void Application::handleKeyDownEvent(SDL_Scancode scancode) {
  if(scancode == SDL_SCANCODE_ESCAPE) {
    SDL_SetRelativeMouseMode(
    SDL_GetRelativeMouseMode() == SDL_TRUE ? SDL_FALSE : SDL_TRUE);
  }
  if(scancode == SDL_SCANCODE_LSHIFT) {
    camera->mode = camera->mode == Camera::FREE_MOVING ?
    Camera::PLATFORMER : Camera::FREE_MOVING;
  }
}
void Application::render() {
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  glDepthMask(GL_FALSE);
  shaderMan->useProgram("skybox");
  if(camera->dirty) {
    shaderMan->setUniform("rotationProjection", "skybox", camera->rotationProjection);
  }
  skybox->render();
  glDepthMask(GL_TRUE);
  shaderMan->useProgram("basic");
  if(camera->dirty) {
    shaderMan->setUniform("view", "basic", camera->view);
    shaderMan->setUniform("viewProjection", "basic", camera->viewProjection);
    shaderMan->setUniform("cameraUnderwater", "basic", (GLint) camera->underwater);
  }
  chunkMan->render();
  SDL_GL_SwapWindow(window);
  camera->dirty = false;
}
void Application::update() {
  handleKeyboardState(SDL_GetKeyboardState(NULL));
  shaderMan->setUniform("currentTime", "basic", (GLfloat) scheduler.currentTime);
  camera->update(scheduler.currentTime);
  chunkMan->update(scheduler.updateInterval);
}
void Application::initialiseSDL() {
  printf("Initialising SDL\n");
  assertFatal(SDL_Init(SDL_INIT_VIDEO) == 0, "%s\n", SDL_GetError());
  #ifdef EMSCRIPTEN
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_ES);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0);
  #else
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, GALOGEN_API_VER_MAJ);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, GALOGEN_API_VER_MIN);
  SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE);
  #endif
  window =
  SDL_CreateWindow("OpenBlocks", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
  WINDOW_SIZE[0], WINDOW_SIZE[1], SDL_WINDOW_OPENGL);
  assertFatal(window != NULL, "%s\n", SDL_GetError());
}
void Application::initialiseOpenGL() {
  printf("Initialising OpenGL context\n");
  context = SDL_GL_CreateContext(window);
  assertFatal(context != NULL, "%s\n", SDL_GetError());
  SDL_GL_SetSwapInterval(0);
  glClearColor(0.0, 0.0, 0.0, 1.0);
  glEnable(GL_BLEND);
  glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
  glEnable(GL_DEPTH_TEST);
  // glEnable(GL_CULL_FACE);
  // glCullFace(GL_BACK);
}
void Application::initialiseOpenGLShaders() {
  printf("Loading OpenGL shaders\n");
  shaderMan = new ShaderManager();
  shaderMan->appendShader("basicVert", GL_VERTEX_SHADER, "assets/shaders/basic.vert");
  shaderMan->appendShader("basicFrag", GL_FRAGMENT_SHADER, "assets/shaders/basic.frag");
  shaderMan->createProgram("basic", {"basicVert", "basicFrag"});
  shaderMan->appendShader("skyboxVert", GL_VERTEX_SHADER, "assets/shaders/skybox.vert");
  shaderMan->appendShader("skyboxFrag", GL_FRAGMENT_SHADER, "assets/shaders/skybox.frag");
  shaderMan->createProgram("skybox", {"skyboxVert", "skyboxFrag"});
}
void Application::initialiseScene() {
  float const ASPECT_RATIO =
  ((float) Application::WINDOW_SIZE[0]) / Application::WINDOW_SIZE[1];
  glm::mat4x4 const projection =
  glm::perspective(45.0f, ASPECT_RATIO, 0.1f, 1000.0f);
  chunkMan = new ChunkManager();
  camera = new Camera(chunkMan, projection);
}
void Application::initialiseTextures() {
  atlas = new TextureAtlas("assets/atlases/new-atlas.png", "assets/atlases/new-atlas.cfg");
  glActiveTexture(GL_TEXTURE0);
  glBindTexture(GL_TEXTURE_2D, atlas->getTexture());
  shaderMan->setUniform("atlas", "basic", 0);
  BlockGenerator::getInstance()->initialise(atlas);
  skybox = new Skybox({
  {.type=GL_TEXTURE_CUBE_MAP_POSITIVE_X, .path="assets/skyboxes/azuresky_right.png"},
  {.type=GL_TEXTURE_CUBE_MAP_NEGATIVE_X, .path="assets/skyboxes/azuresky_left.png"},
  {.type=GL_TEXTURE_CUBE_MAP_POSITIVE_Y, .path="assets/skyboxes/azuresky_top.png"},
  {.type=GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, .path="assets/skyboxes/azuresky_bottom.png"},
  {.type=GL_TEXTURE_CUBE_MAP_POSITIVE_Z, .path="assets/skyboxes/azuresky_back.png"},
  {.type=GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, .path="assets/skyboxes/azuresky_front.png"}});
  glBindTexture(GL_TEXTURE_CUBE_MAP, skybox->getTexture());
  shaderMan->setUniform("cubeMap", "skybox", 0);
}

int main(int argc, char const *argv[]) {
  Application *application = new Application();
  #ifdef EMSCRIPTEN
  auto mainLoop = [](void *arg) {
    Application *application = (Application *) arg;
    SDL_Event event;
    while(SDL_PollEvent(&event)) application->handleEvent(event);
    if(application->scheduler.shouldUpdate()) application->update();
    application->render();
    application->scheduler.FPS += 1;
  };
  emscripten_set_main_loop_arg(mainLoop, (void*) application, 0, 0);
  #else
  while(application->running) {
    SDL_Event event;
    while(SDL_PollEvent(&event)) application->handleEvent(event);
    if(application->scheduler.shouldUpdate()) application->update();
    application->render();
    application->scheduler.FPS += 1;
  }
  delete application;
  return 0;
  #endif
}
