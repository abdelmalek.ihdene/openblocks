#include <cstdio>

#include "include/block_generator.hpp"
#include "include/utility.hpp"

BlockGenerator *BlockGenerator::instance = nullptr;
BlockGenerator *BlockGenerator::getInstance() {
  if(instance == nullptr) instance = new BlockGenerator();
  return instance;
}
void BlockGenerator::initialise(TextureAtlas *atlas) {
  printf("Initialising block factory\n");
  for(int typeIndex = 0; typeIndex < Block::TYPES_NUM; typeIndex += 1) {
    Block::Type type = static_cast<Block::Type>(typeIndex);
    if(type == Block::GRASS) {
      UVP[0][typeIndex] = atlas->getUVPair("grass_side");
      UVP[1][typeIndex] = atlas->getUVPair("grass_side");
      UVP[2][typeIndex] = atlas->getUVPair("grass_top");
      UVP[3][typeIndex] = atlas->getUVPair("grass_bottom");
      UVP[4][typeIndex] = atlas->getUVPair("grass_side");
      UVP[5][typeIndex] = atlas->getUVPair("grass_side");
    } else {
      std::pair<glm::vec2, glm::vec2> face =
      atlas->getUVPair(blockTypeToString(type));
      for(int i = 0; i < 6; i += 1) UVP[i][typeIndex] = face;
    }
  }
}
std::vector<GLfloat> &BlockGenerator::getVertices(float x, float y, float z, Block::Type type, int faceFlags) {
    std::vector<GLfloat> static vertices;
  vertices.clear();
  if(faceFlags == 0) return vertices;
  if(type == Block::LONG_GRASS) {
    vertices.insert(vertices.end(), {
           x,        y,        z, 0, UVP[0][type].second.x, UVP[0][type].second.y,
           x, 1.0f + y,        z, 0, UVP[0][type].second.x, UVP[0][type].first.y,
    1.0f + x, 1.0f + y, 1.0f + z, 0, UVP[0][type].first.x, UVP[0][type].first.y,
    1.0f + x,        y, 1.0f + z, 0, UVP[0][type].first.x, UVP[0][type].second.y,
    1.0f + x,        y,        z, 0, UVP[0][type].second.x, UVP[0][type].second.y,
           x,        y, 1.0f + z, 0, UVP[0][type].first.x, UVP[0][type].second.y,
           x, 1.0f + y, 1.0f + z, 0, UVP[0][type].first.x, UVP[0][type].first.y,
    1.0f + x, 1.0f + y,        z, 0, UVP[0][type].second.x, UVP[0][type].first.y,
    });
  } else {
    if(faceFlags & Face::FRONT) {
      vertices.insert(vertices.end(), {
             x,        y, 1.0f + z, 0, UVP[0][type].second.x, UVP[0][type].second.y,
      1.0f + x,        y, 1.0f + z, 0, UVP[0][type].first.x, UVP[0][type].second.y,
      1.0f + x, 1.0f + y, 1.0f + z, 0, UVP[0][type].first.x, UVP[0][type].first.y,
             x, 1.0f + y, 1.0f + z, 0, UVP[0][type].second.x, UVP[0][type].first.y
      });
    }
    if(faceFlags & Face::BACK) {
      vertices.insert(vertices.end(), {
             x,        y, z, 1, UVP[1][type].first.x, UVP[1][type].second.y,
             x, 1.0f + y, z, 1, UVP[1][type].first.x, UVP[1][type].first.y,
      1.0f + x, 1.0f + y, z, 1, UVP[1][type].second.x, UVP[1][type].first.y,
      1.0f + x,        y, z, 1, UVP[1][type].second.x, UVP[1][type].second.y
      });
    }
    if(faceFlags & Face::TOP) {
      float faceIndex = type == Block::WATER ? 6.0f : 2.0f;
      vertices.insert(vertices.end(), {
             x, 1.0f + y,        z, faceIndex, UVP[2][type].second.x, UVP[2][type].second.y,
             x, 1.0f + y, 1.0f + z, faceIndex, UVP[2][type].first.x, UVP[2][type].second.y,
      1.0f + x, 1.0f + y, 1.0f + z, faceIndex, UVP[2][type].first.x, UVP[2][type].first.y,
      1.0f + x, 1.0f + y,        z, faceIndex, UVP[2][type].second.x, UVP[2][type].first.y
      });
    }
    if(faceFlags & Face::BOTTOM) {
      vertices.insert(vertices.end(), {
             x, y,        z, 3, UVP[3][type].second.x, UVP[3][type].second.y,
      1.0f + x, y,        z, 3, UVP[3][type].first.x, UVP[3][type].second.y,
      1.0f + x, y, 1.0f + z, 3, UVP[3][type].first.x, UVP[3][type].first.y,
             x, y, 1.0f + z, 3, UVP[3][type].second.x, UVP[3][type].first.y,
      });
    }
    if(faceFlags & Face::LEFT) {
      vertices.insert(vertices.end(), {
      x,        y,        z, 4, UVP[4][type].second.x, UVP[4][type].second.y,
      x,        y, 1.0f + z, 4, UVP[4][type].first.x, UVP[4][type].second.y,
      x, 1.0f + y, 1.0f + z, 4, UVP[4][type].first.x, UVP[4][type].first.y,
      x, 1.0f + y,        z, 4, UVP[4][type].second.x, UVP[4][type].first.y
      });
    }
    if(faceFlags & Face::RIGHT) {
      vertices.insert(vertices.end(), {
      1.0f + x,        y,        z, 5, UVP[5][type].first.x, UVP[5][type].second.y,
      1.0f + x, 1.0f + y,        z, 5, UVP[5][type].first.x, UVP[5][type].first.y,
      1.0f + x, 1.0f + y, 1.0f + z, 5, UVP[5][type].second.x, UVP[5][type].first.y,
      1.0f + x,        y, 1.0f + z, 5, UVP[5][type].second.x, UVP[5][type].second.y
      });
    }
  }
  return vertices;
}
std::vector<GLuint> &BlockGenerator::getIndices(GLuint indexOffset, int faceFlags, Block::Type type) {
  std::vector<GLuint> static indices;
  indices.clear();
  if(faceFlags == 0) return indices;
  GLuint j = indexOffset;
  if(type == Block::LONG_GRASS) {
    indices.insert(indices.end(), {
      j, j + 1, j + 2, j, j + 2, j + 3,
      j + 4, j + 5, j + 6, j + 4, j + 6, j + 7
    });
  } else {
    bool faceVisible[6] = {
      (faceFlags & Face::FRONT)   != 0,
      (faceFlags & Face::BACK)    != 0,
      (faceFlags & Face::TOP)     != 0,
      (faceFlags & Face::BOTTOM)  != 0,
      (faceFlags & Face::LEFT)    != 0,
      (faceFlags & Face::RIGHT)   != 0
    };
    for(int i = 0; i < 6; i += 1) {
      if(faceVisible[i] == true) {
        indices.insert(indices.end(), {j, j + 1, j + 2, j, j + 2, j + 3});
        j += 4;
      }
    }
  }
  return indices;
}
std::string BlockGenerator::blockTypeToString(Block::Type type) {
  switch(type) {
    case Block::DIRT: return std::string("dirt");
    case Block::STONE: return std::string("stone");
    case Block::SAND: return std::string("sand");
    case Block::SNOW: return std::string("snow");
    case Block::LONG_GRASS: return std::string("long_grass");
    case Block::WATER: return std::string("water");
    case Block::WOOD: return std::string("wood");
    case Block::LEAVES: return std::string("leaves");
    default: return std::string("unknown");
  }
}
