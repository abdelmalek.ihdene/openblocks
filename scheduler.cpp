#include <SDL2/SDL.h>

#include "include/scheduler.hpp"

Scheduler::Scheduler(double updateInterval) {
  this->updateInterval = updateInterval;
}
Scheduler::~Scheduler() {
  return;
}
bool Scheduler::shouldUpdate() {
  currentTime = ((double) SDL_GetTicks()) / 1000.0;
  deltaTime = currentTime - lastTime;
  lastTime = currentTime;
  secondAccumulator += deltaTime;
  updateAccumulator += deltaTime;
  if(secondAccumulator >= 1) {
    secondAccumulator -= 1;
    printf("FPS: %d\n", FPS);
    FPS = 0;
  }
  if(updateAccumulator >= updateInterval) {
    updateAccumulator -= updateInterval;
    return true;
  }
  return false;
}
