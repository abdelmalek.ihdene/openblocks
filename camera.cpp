#include <cstdio>
#include <glm/gtc/constants.hpp>
#include <glm/mat4x4.hpp>
#include <glm/vec3.hpp>

#include "include/camera.hpp"

Camera::Camera(ChunkManager *chunkMan, glm::mat4 projection) {
  this->chunkMan = chunkMan;
  chunkMan->camera = this;
  this->projection = projection;
  right = glm::cross(forward, Camera::WORLD_UP);
  view =
  glm::lookAt(position + EYE_POSITION, position + EYE_POSITION + forward, WORLD_UP);
  viewProjection =
  projection * view;
  rotationProjection =
  projection * glm::rotate(glm::mat4(1.0), -1 * pitch, right) *
  glm::rotate(glm::mat4(1.0), -1 * yaw, WORLD_UP);
  frustrum.update(projection, view);
  dirty = true;
}
Camera::~Camera() {
  return;
}
void Camera::applyGravity() {
  if(mode == FREE_MOVING) return;
  float const gravityAccel = 0.04;
  float const underwaterGravityAccel = 0.01;
  float const minYVel = -1.0;
  if(underwater) yVel -= underwaterGravityAccel;
  else yVel -= gravityAccel;
  if(yVel < minYVel) yVel = minYVel;
  Block block = chunkMan->getBlock(position.x, position.y, position.z);
  if(block.type != Block::WATER && block.active && yVel < 0) {
    jumpAvailable = true;
    yVel = 0;
  }
  position.y += yVel;
}
void Camera::update(double currentTime) {
  updateUnderwaterFlag(currentTime);
  applyGravity();
  view =
  glm::lookAt(position + EYE_POSITION, position + EYE_POSITION + forward, WORLD_UP);
  viewProjection =
  projection * view;
  frustrum.update(projection, view);
  dirty = true;
}
void Camera::updateUnderwaterFlag(double currentTime) {
  Block waterBlock = chunkMan->getBlock(position.x, 15, position.z);
  float waterSurfaceY = 16 + glm::min(0.15 * glm::sin((int) position.x - 0.1 + currentTime / 1.8f) +
  0.15 * glm::sin((int) position.z + 0.1 + currentTime / 1.3f), 0.15) - 0.4;
  underwater =
  waterBlock.active == true &&
  waterBlock.type == Block::WATER &&
  waterSurfaceY > position.y + EYE_POSITION.y;
}
void Camera::move(MOVEMENT_OPTION direction) {
  auto blockActiveAndCollideable = [](Block block) {
    return block.active == true &&
    block.type != Block::LONG_GRASS &&
    block.type != Block::WATER;
  };
  glm::vec3 ray;
  float const FREE_MOVING_CAMERA_SPEED = 1.2;
  float const PLATFORMER_CAMERA_SPEED = 0.4;
  Block block;
  if(mode == FREE_MOVING) {
    switch(direction) {
      case MOVE_FORWARD: position += FREE_MOVING_CAMERA_SPEED * forward; break;
      case MOVE_BACKWARD: position -= FREE_MOVING_CAMERA_SPEED * forward; break;
      case MOVE_RIGHT: position += FREE_MOVING_CAMERA_SPEED * right; break;
      case MOVE_LEFT: position -= FREE_MOVING_CAMERA_SPEED * right; break;
      case JUMP: break;
    }
  } else if(mode == PLATFORMER) {
    switch(direction) {
      case MOVE_FORWARD:
        ray = position + 0.8f * glm::vec3(forward.x, 0, forward.z);
        ray.y = glm::ceil(ray.y);
        if(blockActiveAndCollideable(chunkMan->getBlock(ray.x, ray.y, ray.z)) == false)
          position += PLATFORMER_CAMERA_SPEED * glm::vec3(forward.x, 0, forward.z);
        break;
      case MOVE_BACKWARD:
        ray = position - 0.8f * glm::vec3(forward.x, 0, forward.z);
        ray.y = glm::ceil(ray.y);
        if(blockActiveAndCollideable(chunkMan->getBlock(ray.x, ray.y, ray.z)) == false)
          position -= PLATFORMER_CAMERA_SPEED * glm::vec3(forward.x, 0, forward.z);
        break;
      case MOVE_RIGHT:
        ray = position + 0.8f * glm::vec3(right.x, 0, right.z);
        ray.y = glm::ceil(ray.y);
        if(blockActiveAndCollideable(chunkMan->getBlock(ray.x, ray.y, ray.z)) == false)
          position += PLATFORMER_CAMERA_SPEED * glm::vec3(right.x, 0, right.z);
        break;
      case MOVE_LEFT:
        ray = position - 0.8f * glm::vec3(right.x, 0, right.z);
        ray.y = glm::ceil(ray.y);
        if(blockActiveAndCollideable(chunkMan->getBlock(ray.x, ray.y, ray.z)) == false)
          position -= PLATFORMER_CAMERA_SPEED * glm::vec3(right.x, 0, right.z);
        break;
      case JUMP:
        if(jumpAvailable) {
          jumpAvailable = false;
          yVel = 0.65;
        }
        break;
    }
  }
  view =
  lookAt(position, position + forward, WORLD_UP);
  viewProjection =
  projection * view;
  frustrum.update(projection, view);
  dirty = true;
}
void Camera::rotate(glm::vec2 mouseDelta) {
  float const PI = glm::pi<float>();
  yaw -= mouseDelta.x * MOUSE_SENSITIVITY;
  pitch -= mouseDelta.y * MOUSE_SENSITIVITY;
  if(yaw > 2 * PI) yaw -= 2 * PI;
  else if(yaw < 0) yaw += 2 * PI;
  pitch = glm::clamp(pitch, -6 * PI / 13, 6 * PI / 13);
  forward = glm::mat3(glm::rotate(glm::mat4(1.0), yaw, WORLD_UP)) * glm::vec3(0, 0, -1);
  glm::mat4 rotation = glm::rotate(glm::mat4(1.0), -1 * yaw, WORLD_UP);
  right = glm::cross(forward, Camera::WORLD_UP);
  rotation = rotation * glm::rotate(glm::mat4(1.0), -1 * pitch, right);
  forward = glm::mat3(glm::rotate(glm::mat4(1.0), pitch, right)) * forward;
  view = glm::lookAt(position + EYE_POSITION, position + EYE_POSITION + forward, WORLD_UP);
  rotationProjection =
  projection * rotation;
  viewProjection =
  projection * view;
  frustrum.update(projection, view);
  dirty = true;
}
