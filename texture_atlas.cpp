#include <cstdio>
#include <fstream>

#include "vendor/include/stb_image.h"

#include "include/texture_atlas.hpp"
#include "include/utility.hpp"

TextureAtlas::TextureAtlas(std::string const &image, std::string const &config) {
  loadImage(image);
  loadConfig(config);
}
TextureAtlas::~TextureAtlas() {
  glDeleteTextures(1, &texture);
}
std::pair<glm::vec2, glm::vec2> TextureAtlas::getUVPair(std::string const &name) {
  auto item = uv.find(name);
  assertFatal(item != uv.end(),
  "Could not get UV coordinates of subtexture %s\n", name.c_str());
  return item->second;
}
GLuint TextureAtlas::getTexture() {
  return texture;
}
void TextureAtlas::loadConfig(std::string const &path) {
  printf("Loading atlas configuration %s\n", path.c_str());
  std::ifstream inputFileStream(path);
  assertFatal(inputFileStream.is_open(), "Could not open atls configuration %s\n", path.c_str());
  std::string line;
  std::getline(inputFileStream, line);
  int uvCoordinateCount = 0;
  assertFatal(sscanf(line.c_str(), "count %d", &uvCoordinateCount) == 1,
  "Could not parse line %s of atlas configuration %s\n", line.c_str(), path.c_str());
  printf("Found (%d) subtextures\n", uvCoordinateCount);
  for(int i = 0; i < uvCoordinateCount; i += 1) {
    std::getline(inputFileStream, line);
    char name[64];
    assertFatal(sscanf(line.c_str(), "name %64s", name) == 1,
    "Could not parse line %s of atlas configuration %s\n", line.c_str(), path.c_str());
    std::getline(inputFileStream, line);
    glm::vec2 TL, BR;
    assertFatal(sscanf(line.c_str(), "uv %f %f %f %f", &TL.x, &TL.y, &BR.x, &BR.y) == 4,
    "Could not parse line %s of atlas configuration %s\n", line.c_str(), path.c_str());
    printf("Found subtexture %s <%g, %g, %g, %g>\n", name, TL.x, TL.y, BR.x, BR.y);
    uv[std::string(name)] = std::make_pair(TL, BR);
  }
  inputFileStream.close();
}
void TextureAtlas::loadImage(std::string const &path) {
  int channelCount = 0;
  int height = 0;
  int width = 0;
  printf("Loading GL_TEXTURE_2D %s\n", path.c_str());
  unsigned char *image = stbi_load(path.c_str(), &width, &height, &channelCount, 0);
  assertFatal(image != NULL, "Image %s could not be loaded\n", path.c_str());
  GLenum format = 0;
  switch(channelCount) {
  case 3: format = GL_RGB; break;
  case 4: format = GL_RGBA; break;
  default: assertFatal(false, "Image %s has less than 3 channels (%d)\n", path.c_str(), channelCount);}
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_2D, texture);
  glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
  stbi_image_free(image);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
  glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAX_LEVEL, 4);
  glGenerateMipmap(GL_TEXTURE_2D);
}
