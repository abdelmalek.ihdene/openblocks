#include "vendor/include/stb_image.h"

#include "include/skybox.hpp"
#include "include/utility.hpp"

Skybox::Skybox(std::initializer_list<struct CubeMapTextureInfo> textures) :
RenderObject({{.index=0, .size=3, .type=GL_FLOAT, .normalized=GL_FALSE}}) {
  loadCubeMap(textures);
  std::vector<GLfloat> vertices = {
    -1.0f, -1.0f, -1.0f,
    1.0f, -1.0f, -1.0f,
    1.0f, 1.0f, -1.0f,
    -1.0f, 1.0f, -1.0f,
    -1.0f, -1.0f, 1.0f,
    1.0f, -1.0f, 1.0f,
    1.0f, 1.0f, 1.0f,
    -1.0f, 1.0f, 1.0f
  };
  updateVertices(vertices);
  std::vector<GLuint> indices = {
    0, 1, 3, 3, 1, 2,
    1, 5, 2, 2, 5, 6,
    5, 4, 6, 6, 4, 7,
    4, 0, 7, 7, 0, 3,
    3, 2, 7, 7, 2, 6,
    4, 5, 0, 0, 5, 1
  };
  updateIndices(indices);
}
Skybox::~Skybox() {
  glDeleteTextures(1, &texture);
}
void Skybox::loadCubeMap(std::initializer_list<struct CubeMapTextureInfo> textures) {
  auto cubeMapTextureTypeToString = [](GLenum cubeMapTextureType) {
    switch(cubeMapTextureType) {
      case GL_TEXTURE_CUBE_MAP_POSITIVE_X: return std::string("GL_TEXTURE_CUBE_MAP_POSITIVE_X");
      case GL_TEXTURE_CUBE_MAP_NEGATIVE_X: return std::string("GL_TEXTURE_CUBE_MAP_NEGATIVE_X");
      case GL_TEXTURE_CUBE_MAP_POSITIVE_Y: return std::string("GL_TEXTURE_CUBE_MAP_POSITIVE_Y");
      case GL_TEXTURE_CUBE_MAP_NEGATIVE_Y: return std::string("GL_TEXTURE_CUBE_MAP_NEGATIVE_Y");
      case GL_TEXTURE_CUBE_MAP_POSITIVE_Z: return std::string("GL_TEXTURE_CUBE_MAP_POSITIVE_Z");
      case GL_TEXTURE_CUBE_MAP_NEGATIVE_Z: return std::string("GL_TEXTURE_CUBE_MAP_NEGATIVE_Z");
      default:
        assertFatal(false, "Specified cube map texture type is not valid\n");
        return std::string("");
    }
  };
  glGenTextures(1, &texture);
  glBindTexture(GL_TEXTURE_CUBE_MAP, texture);
  for(auto texture : textures) {
    int channelCount = 0;
    int height = 0;
    int width = 0;
    printf("Loading GL_TEXTURE_CUBE_MAP<%s> %s\n", cubeMapTextureTypeToString(texture.type).c_str(), texture.path.c_str());
    unsigned char *image = stbi_load(texture.path.c_str(), &width, &height, &channelCount, 0);
    assertFatal(image != NULL, "Image %s could not be loaded\n", texture.path.c_str());
    GLenum format = 0;
    switch(channelCount) {
    case 3: format = GL_RGB; break;
    case 4: format = GL_RGBA; break;
    default: assertFatal(false, "Image %s has less than 3 channels (%d)\n", texture.path.c_str(), channelCount);}
    glTexImage2D(texture.type, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, image);
    stbi_image_free(image);
  }
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
  glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
}
GLuint Skybox::getTexture() {
  return texture;
}
