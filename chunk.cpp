#include <cstdio>
#include <glm/glm.hpp>

#include "vendor/include/SimplexNoise.h"

#include "include/block_generator.hpp"
#include "include/chunk.hpp"

Chunk::Chunk() :
RenderObject({{.index=0, .size=3, .type=GL_FLOAT, .normalized=GL_FALSE},
{.index=1, .size=1, .type=GL_FLOAT, .normalized=GL_FALSE},
{.index=2, .size=2, .type=GL_FLOAT, .normalized=GL_FALSE}}) {
  return;
}
Chunk::~Chunk() {
  return;
}
void Chunk::updateMesh() {
  std::vector<GLuint> static chunkIndices;
  std::vector<GLfloat> static chunkVertices;
  chunkIndices.clear();
  chunkVertices.clear();
  std::vector<glm::vec3> static waterBlocks;
  waterBlocks.clear();
  BlockGenerator *blockGenerator = BlockGenerator::getInstance();
  GLuint indexOffset = 0;
  for(int i = 0; i < CHUNK_SIZE; i += 1) {
    for(int j = 0; j < CHUNK_SIZE; j += 1) {
      for(int k = 0; k < CHUNK_SIZE; k += 1) {
        if(blocks[i][j][k].active) {
          int faceCount = 0;
          int faceFlags = 0;
          if(blocks[i][j][k].type != Block::WATER) {
            if(i == 0 || blocks[i - 1][j][k].active != true
            || blocks[i - 1][j][k].type == Block::LONG_GRASS
            || blocks[i - 1][j][k].type == Block::WATER) {
              faceFlags |= BlockGenerator::Face::LEFT;
              faceCount += 1;
            }
            if(i == (CHUNK_SIZE - 1) || blocks[i + 1][j][k].active != true
            || blocks[i + 1][j][k].type == Block::LONG_GRASS
            || blocks[i + 1][j][k].type == Block::WATER) {
              faceFlags |= BlockGenerator::Face::RIGHT;
              faceCount += 1;
            }
            if(j == 0 || blocks[i][j - 1][k].active != true
              || blocks[i][j - 1][k].type == Block::LONG_GRASS
              || blocks[i][j - 1][k].type == Block::WATER) {
              faceFlags |= BlockGenerator::Face::BOTTOM;
              faceCount += 1;
            }
            if(j == (CHUNK_SIZE - 1) || blocks[i][j + 1][k].active != true
            || blocks[i][j + 1][k].type == Block::LONG_GRASS
            || blocks[i][j + 1][k].type  == Block::WATER) {
              faceFlags |= BlockGenerator::Face::TOP;
              faceCount += 1;
            }
            if(k == 0 || blocks[i][j][k - 1].active != true
            || blocks[i][j][k - 1].type == Block::LONG_GRASS
            || blocks[i][j][k - 1].type == Block::WATER) {
              faceFlags |= BlockGenerator::Face::BACK;
              faceCount += 1;
            }
            if(k == (CHUNK_SIZE - 1) || blocks[i][j][k + 1].active != true
            || blocks[i][j][k + 1].type == Block::LONG_GRASS
            || blocks[i][j][k + 1].type == Block::WATER) {
              faceFlags |= BlockGenerator::Face::FRONT;
              faceCount += 1;
            }
            std::vector<GLuint> &indices = blockGenerator->getIndices(indexOffset, faceFlags, blocks[i][j][k].type);
            indexOffset += blocks[i][j][k].type == Block::LONG_GRASS ? 8 : 4 * faceCount;
            std::vector<GLfloat> &vertices = blockGenerator->getVertices(
            position.x + i, position.y + j, position.z + k, blocks[i][j][k].type, faceFlags);
            chunkIndices.insert(chunkIndices.end(), indices.begin(), indices.end());
            chunkVertices.insert(chunkVertices.end(), vertices.begin(), vertices.end());
          } else {
            waterBlocks.push_back(glm::vec3(i, j, k));
          }
        }
      }
    }
  }
  waterBlockOffset = chunkIndices.size();
  for(auto indexes : waterBlocks) {
    std::vector<GLuint> &indices = blockGenerator->getIndices(indexOffset, BlockGenerator::Face::TOP, Block::WATER);
    indexOffset += 4;
    std::vector<GLfloat> &vertices = blockGenerator->getVertices(
    position.x + indexes.x, position.y + indexes.y, position.z + indexes.z, Block::WATER, BlockGenerator::Face::TOP);
    chunkIndices.insert(chunkIndices.end(), indices.begin(), indices.end());
    chunkVertices.insert(chunkVertices.end(), vertices.begin(), vertices.end());
  }
  chunkIndicesCount = chunkIndices.size();
  updateIndices(chunkIndices);
  updateVertices(chunkVertices);
}
void Chunk::renderOpaque() {
  render(0, waterBlockOffset);
}
void Chunk::renderWater() {
  if(chunkIndicesCount - waterBlockOffset == 0) return;
  render(waterBlockOffset, chunkIndicesCount - waterBlockOffset);
}
