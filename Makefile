clang:
	clang++ $(wildcard *.cpp vendor/*.c vendor/*.cpp vendor/*.a) \
	-g -lGL -o out -Wall -Werror=format-security -Wno-unused-variable \
	`pkg-config --cflags --libs sdl2` -Ivendor/include -O3
em++:
	em++ $(wildcard *.cpp vendor/stb_image.c vendor/*.cpp) \
	-o web/index.html -s USE_SDL=2 -s FULL_ES3=1 -s USE_WEBGL2=1 \
	-Ivendor/include --preload-file assets -s ALLOW_MEMORY_GROWTH=1 -O3
