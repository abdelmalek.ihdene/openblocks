#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <ctime>
#include <glm/glm.hpp>
#include "vendor/include/SimplexNoise.h"

#include "include/chunk_manager.hpp"
#include "include/camera.hpp"

int32_t static int16PairToInt32(int16_t a, int16_t b) {
  // https://stackoverflow.com/questions/1294649/cleanest-way-to-combine-two-shorts-to-an-int
  return ((a << 16) | (b & 0xffff));
}

ChunkManager::ChunkManager() {
  printf("Creating chunk manager\n");
  SimplexNoise::shufflePermutationTable();
  srand(time(NULL));
}
ChunkManager::~ChunkManager() {
  for(auto entry : chunks) delete entry.second;
}
void ChunkManager::render() {
  for(auto item : chunks) {
    if(item.second != nullptr && item.second->active) {
      int16_t x = item.first >> 16;
      int16_t z = item.first & 0xffff;
      item.second->renderOpaque();
    }
  }
  for(auto item : chunks) {
    if(item.second != nullptr && item.second->active) item.second->renderWater();
  }
}
void ChunkManager::update(double deltaTime) {
  for(auto item : chunks) {
    if(item.second != nullptr) item.second->active = false;
  }
  accumulator += deltaTime;
  if(accumulator >= 1.0 / MAX_CHUNKS_GENERATED_PER_SECOND) {
    accumulator -= 1.0 / MAX_CHUNKS_GENERATED_PER_SECOND;
    if(chunkGenerationQueue.size() > 0) {
      std::pair<int16_t, int16_t> pair = chunkGenerationQueue.back();
      generateChunk(pair.first, pair.second);
      chunkGenerationQueue.pop_back();
    }
  }
  int16_t gridX = ((int16_t) glm::round((float) camera->position.x / Chunk::CHUNK_SIZE)) * Chunk::CHUNK_SIZE;
  int16_t gridZ = ((int16_t) glm::round((float) camera->position.z / Chunk::CHUNK_SIZE)) * Chunk::CHUNK_SIZE;
  int const range[2][2] = {{4, 4}, {4, 4}};
  int const positionsLength = (range[0][0] + range[0][1]) * (range[1][0] + range[1][1]);
  int16_t positions[positionsLength][3] = {{0}};
  int16_t const chunkSize = Chunk::CHUNK_SIZE;
  int index = 0;
  for(int16_t i = gridX - chunkSize * range[0][0]; i < gridX + chunkSize * range[0][1]; i += chunkSize) {
    for(int16_t j = gridZ - chunkSize * range[1][0]; j < gridZ + chunkSize * range[1][1]; j += chunkSize) {
      positions[index][0] = i;
      positions[index][1] = j;
      positions[index][2] = 0;
      index += 1;
    }
  }
  bool swapped = false;
  for(int i = 0; i < positionsLength - 1; i += 1) {
    swapped = false;
    for(int j = 0; j < positionsLength - i - 1; j += 1) {
      auto distanceCompare = [](int16_t x, int16_t z, int16_t ax, int16_t az, int16_t bx, int16_t bz) {
        return glm::distance(glm::vec2(x, z), glm::vec2(ax, az)) < glm::distance(glm::vec2(x, z), glm::vec2(bx, bz));
      };
      if(distanceCompare(camera->position.x, camera->position.z,
        positions[j][0], positions[j][1], positions[j + 1][0], positions[j + 1][1])) {
        int16_t t[2] = {positions[j][0], positions[j][1]};
        positions[j][0] = positions[j + 1][0];
        positions[j][1] = positions[j + 1][1];
        positions[j + 1][0] = t[0];
        positions[j + 1][1] = t[1];
        swapped = true;
      }
    }
    if (swapped == false) break;
  }
  bool static initialised = false;
  if(initialised == true) {
    for(int i = 0; i < positionsLength; i += 1) {
      bool visible =
      camera->frustrum.isInside(std::make_pair(glm::vec3(positions[i][0], 0, positions[i][1]),
      glm::vec3(positions[i][0] + Chunk::CHUNK_SIZE, Chunk::CHUNK_SIZE, positions[i][1] + Chunk::CHUNK_SIZE)));
      if(visible) positions[i][2] = 1;
    }
  } else {
    for(int i = 0; i < positionsLength; i += 1) {
      positions[i][2] = 1;
    }
  }
  for(int i = 0; i < positionsLength; i += 1) {
    if(positions[i][2] == 0) continue;
    auto pair = std::make_pair(positions[i][0], positions[i][1]);
    auto chunksItem = chunks.find(int16PairToInt32(positions[i][0], positions[i][1]));
    if(chunksItem == chunks.end()) {
      auto chunkGenerationQueueItem =
      std::find(chunkGenerationQueue.begin(), chunkGenerationQueue.end(), pair);
      if(chunkGenerationQueueItem == chunkGenerationQueue.end()) {
        if(initialised) chunkGenerationQueue.push_front(pair);
        else chunkGenerationQueue.push_back(pair);
        chunks[int16PairToInt32(positions[i][0], positions[i][1])] = nullptr;
      }
    } else {
      if(chunksItem->second != nullptr) chunksItem->second->active = true;
    }
  }
  initialised = true;
}
void ChunkManager::generateChunk(int16_t x, int16_t z) {
  float const fractalScalingFactor = 0.008;
  int const stoneDepth = 8;
  Chunk *chunk = new Chunk();
  chunk->position = glm::vec3(x, 0, z);
  int values[Chunk::CHUNK_SIZE][Chunk::CHUNK_SIZE];
  for(int i = 0; i < Chunk::CHUNK_SIZE; i += 1) {
    for(int k = 0; k < Chunk::CHUNK_SIZE; k += 1) {
      float value = SimplexNoise::fractal(8, (x + i) * fractalScalingFactor, (z + k) * fractalScalingFactor);
      value = ((value - (-1)) * (Chunk::CHUNK_SIZE - 0)) / (1 - (-1)) + 0;
      values[i][k] = glm::round(value);
    }
  }
  int randomValue = 0;
  for(int i = 0; i < Chunk::CHUNK_SIZE; i += 1) {
    for(int j = Chunk::CHUNK_SIZE - 1; j > 0; j -= 1) {
      for(int k = 0; k < Chunk::CHUNK_SIZE; k += 1) {
        randomValue = rand() % 300;
        if(j > 18 && j == values[i][k] + 1) {
          if(j < Chunk::CHUNK_SIZE - 20 && randomValue > 285) {
            chunk->blocks[i][j][k].active = true;
            chunk->blocks[i][j][k].type = Block::LONG_GRASS;
          }
        } else if(j <= values[i][k]) {
          chunk->blocks[i][j][k].active = true;
          if(j == values[i][k]) {
            if(j > Chunk::CHUNK_SIZE - 13) chunk->blocks[i][j][k].type = Block::SNOW;
            else if(j > Chunk::CHUNK_SIZE - 14 && randomValue > 60) chunk->blocks[i][j][k].type = Block::SNOW;
            else if(j > Chunk::CHUNK_SIZE - 15 && randomValue > 285) chunk->blocks[i][j][k].type = Block::SNOW;
            else if(j <= 17 && randomValue > 60) chunk->blocks[i][j][k].type = Block::SAND;
            else if(j <= 16) chunk->blocks[i][j][k].type = Block::SAND;
            else chunk->blocks[i][j][k].type = Block::GRASS;
          } else if(j >= values[i][k] - stoneDepth) chunk->blocks[i][j][k].type = Block::DIRT;
          else chunk->blocks[i][j][k].type = Block::STONE;
        } else if(j == 15) {
          chunk->blocks[i][j][k].active = true;
          chunk->blocks[i][j][k].type = Block::WATER;
        } else {
          chunk->blocks[i][j][k].active = false;
        }
      }
    }
  }
  chunk->updateMesh();
  chunks[int16PairToInt32(x, z)] = chunk;
}
Block ChunkManager::getBlock(float x, float y, float z) {
  if(y > Chunk::CHUNK_SIZE) return Block(false, Block::DIRT);
  Chunk static *chunk = nullptr;
  int32_t static lastKey = 0;
  int16_t gridX = ((int16_t) glm::floor(x / Chunk::CHUNK_SIZE)) * Chunk::CHUNK_SIZE;
  int16_t gridZ = ((int16_t) glm::floor(z / Chunk::CHUNK_SIZE)) * Chunk::CHUNK_SIZE;
  int32_t key = int16PairToInt32(gridX, gridZ);
  if(chunk == nullptr || key != lastKey) {
    auto chunksItem = chunks.find(key);
    if(chunksItem != chunks.end() && chunksItem->second != nullptr) {
      chunk = chunksItem->second;
      lastKey = key;
    } else return Block(false, Block::DIRT);
  }
  int i = x - gridX;
  int j = y;
  int k = z - gridZ;
  return chunk->blocks[i][j][k];
}
