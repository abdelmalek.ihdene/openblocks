#pragma once

#include <cstdint>
#include <glm/vec2.hpp>
#include <deque>
#include <unordered_map>
#include <utility>

#include "chunk.hpp"

class Camera;
class ChunkManager {
public:
  int const MAX_CHUNKS_GENERATED_PER_SECOND = 5;
public:
  Camera *camera = nullptr;
public:
  ChunkManager();
  ~ChunkManager();
public:
  Block getBlock(float x, float y, float z);
  void render();
  void update(double deltaTime);
private:
  void generateChunk(int16_t x, int16_t z);
private:
  double accumulator = 0;
  std::unordered_map<int32_t, Chunk*> chunks;
  std::deque<std::pair<int16_t, int16_t>> chunkGenerationQueue;
};
