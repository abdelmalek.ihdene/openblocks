#pragma once

class Block {
public:
  enum Type {
    DIRT = 0,
    GRASS,
    STONE,
    SAND,
    SNOW,
    LONG_GRASS,
    WATER,
    WOOD,
    LEAVES,
    TYPES_NUM
  };
public:
  bool active = false;
  Type type = Type::DIRT;
public:
  Block();
  Block(bool active, Type type);
  ~Block();
};
