#pragma once

#include "camera.hpp"
#include "chunk_manager.hpp"
#include "render_object.hpp"
#include "scheduler.hpp"
#include "shader_manager.hpp"
#include "skybox.hpp"
#include "texture_atlas.hpp"

class Application {
public:
  int const WINDOW_SIZE[2] = {1024, 600};
  double const UPDATE_INTERVAL = 1.0 / 30.0;
public:
  SDL_GLContext context;
  bool running = true;
  Scheduler scheduler = Scheduler(UPDATE_INTERVAL);
  SDL_Window *window = nullptr;
public:
  TextureAtlas *atlas = nullptr;
  Camera *camera = nullptr;
  ChunkManager *chunkMan = nullptr;
  ShaderManager *shaderMan = nullptr;
  Skybox *skybox = nullptr;
public:
  Application();
  ~Application();
public:
  void handleEvent(SDL_Event &event);
  void handleKeyboardState(unsigned char const *state);
  void handleKeyDownEvent(SDL_Scancode scancode);
  void render();
  void update();
  void updateUniforms();
private:
  void initialiseCamera();
  void initialiseSDL();
  void initialiseOpenGL();
  void initialiseOpenGLShaders();
  void initialiseScene();
  void initialiseTextures();
};
