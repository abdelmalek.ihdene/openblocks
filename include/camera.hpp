#pragma once

#include <glm/mat4x4.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/vec2.hpp>
#include <glm/vec3.hpp>

#include "chunk_manager.hpp"
#include "frustrum.hpp"

class Camera {
public:
  enum MOVEMENT_OPTION {
    MOVE_FORWARD,
    MOVE_BACKWARD,
    MOVE_RIGHT,
    MOVE_LEFT,
    JUMP
  };
  enum CAMERA_MODE {
    FREE_MOVING,
    PLATFORMER
  } mode = PLATFORMER;
public:
  float const PI = glm::pi<float>();
  float const MOUSE_SENSITIVITY = 0.01;
  glm::vec3 const EYE_POSITION = glm::vec3(0, 2.5f, 0);
  glm::vec3 const WORLD_UP = glm::vec3(0, 1, 0);
public:
  ChunkManager *chunkMan;
  Frustrum frustrum;
public:
  bool dirty = true;
  bool jumpAvailable = false;
  bool underwater = false;
public:
  float pitch = 0.0f;
  float yaw = 0.0f;
  float yVel = 0;
public:
  glm::vec3 forward = glm::vec3(0, 0, -1);
  glm::vec3 position = glm::vec3(0.5, 300, 0.5);
  glm::vec3 right = glm::vec3(0, 0, 0);
public:
  glm::mat4 projection;
  glm::mat4 rotationProjection;
  glm::mat4 viewProjection;
  glm::mat4 view;
public:
  Camera(ChunkManager *chunkMan, glm::mat4 projection);
  ~Camera();
public:
  void applyGravity();
  void move(MOVEMENT_OPTION direction);
  void rotate(glm::vec2 mouseDelta);
  void update(double currentTime);
  void updateUnderwaterFlag(double currentTime);
};
