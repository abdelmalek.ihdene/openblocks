#pragma once

#include <initializer_list>
#include <string>

#ifdef EMSCRIPTEN
#include <GLES3/gl3.h>
#else
#include <gl.h>
#endif

#include "render_object.hpp"

class Skybox : public RenderObject {
public:
  struct CubeMapTextureInfo {
    GLenum type;
    std::string path;
  };
public:
  Skybox(std::initializer_list<struct CubeMapTextureInfo> textures);
  ~Skybox();
public:
  void loadCubeMap(std::initializer_list<struct CubeMapTextureInfo> textures);
  GLuint getTexture();
private:
  GLuint texture = 0;
};
