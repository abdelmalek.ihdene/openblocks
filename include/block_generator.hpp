#pragma once

#include <array>
#include <vector>

#ifdef EMSCRIPTEN
#include <GLES3/gl3.h>
#else
#include <gl.h>
#endif

#include "block.hpp"
#include "texture_atlas.hpp"

class BlockGenerator {
public:
  enum Face {
    FRONT     = 1 << 0,
    BACK      = 1 << 1,
    TOP       = 1 << 2,
    BOTTOM    = 1 << 3,
    LEFT      = 1 << 4,
    RIGHT     = 1 << 5,
  };
public:
  int static const CUBE_FACES = FRONT | BACK | TOP | BOTTOM | LEFT | RIGHT;
public:
  BlockGenerator static *getInstance();
public:
  std::string blockTypeToString();
  void initialise(TextureAtlas *atlas);
  std::vector<GLfloat> &getVertices(float x, float y, float z, Block::Type type, int faceFlags);
  std::vector<GLuint> &getIndices(GLuint indexOffset, int faceFlags, Block::Type type);
  std::string blockTypeToString(Block::Type type);
private:
  std::pair<glm::vec2, glm::vec2> UVP[6][Block::TYPES_NUM];
  BlockGenerator static *instance;
private:
  BlockGenerator() {};
};
