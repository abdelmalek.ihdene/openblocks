#pragma once

#include <glm/vec3.hpp>

#ifdef EMSCRIPTEN
#include <GLES3/gl3.h>
#else
#include <gl.h>
#endif

#include "block.hpp"
#include "render_object.hpp"

class Chunk: public RenderObject {
public:
  int static const CHUNK_SIZE = 60;
public:
  bool active = true;
  GLuint waterBlockOffset = 0;
  GLuint chunkIndicesCount = 0;
  Block blocks[CHUNK_SIZE][CHUNK_SIZE][CHUNK_SIZE];
  glm::vec3 position = glm::vec3(0, 0, 0);
public:
  Chunk();
  ~Chunk();
public:
  void updateMesh();
  void renderOpaque();
  void renderWater();
};
