#pragma once

#include <string>
#include <unordered_map>

#ifdef EMSCRIPTEN
#include <GLES3/gl3.h>
#else
#include <gl.h>
#endif

class TextureManager {
public:
  struct Texture {
    GLenum format;
    GLuint identifier;
    int size[2];
    GLenum type;
  };
public:
  TextureManager();
  ~TextureManager();
public:
  void loadTexture(std::string const &name, GLenum type, std::string const &path);
  struct Texture &getTexture(std::string const &name);
  void bindTextureToUnit(GLint unit, std::string const &name);
private:
  std::unordered_map<std::string, struct Texture> textures;
private:
  std::string textureTypeToString(GLenum type);
};
