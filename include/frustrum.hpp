#pragma once

#include <glm/mat4x4.hpp>
#include <utility>

class Frustrum {
public:
  enum Plane { RIGHT, LEFT, BOTTOM, TOP, FRONT, BACK };
  enum { A, B, C, D };
public:
  Frustrum();
  ~Frustrum();
public:
  void update(glm::mat4 projection, glm::mat4 view);
  bool isInside(std::pair<glm::vec3, glm::vec3> box);
  bool isInside(glm::vec3 point);
private:
  glm::vec4 getPlane(Plane plane);
  void normalize(Plane plane);
private:
  float data[6][4];
};
