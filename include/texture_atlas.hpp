#pragma once

#include <glm/vec2.hpp>
#include <string>
#include <unordered_map>
#include <utility>

#ifdef EMSCRIPTEN
#include <GLES3/gl3.h>
#else
#include <gl.h>
#endif

class TextureAtlas {
public:
  TextureAtlas(std::string const &image, std::string const &config);
  ~TextureAtlas();
public:
  std::pair<glm::vec2, glm::vec2> getUVPair(std::string const &name);
  GLuint getTexture();
private:
  void loadConfig(std::string const &path);
  void loadImage(std::string const &path);
private:
  GLuint texture;
  std::unordered_map<std::string, std::pair<glm::vec2, glm::vec2>> uv;
};
