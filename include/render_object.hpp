#pragma once

#include <initializer_list>
#include <vector>

#ifdef EMSCRIPTEN
#include <GLES3/gl3.h>
#else
#include <gl.h>
#endif

class RenderObject {
public:
  struct OGLVertexAttribute {
    GLuint index;
    GLint size;
    GLenum type;
    GLboolean normalized;
  };
public:
  RenderObject(std::initializer_list<struct OGLVertexAttribute> attributes);
  ~RenderObject();
public:
  void render(GLuint indexOffset = 0, GLuint indexCount = 0);
public:
  void updateIndices(std::vector<GLuint> &indices);
  void updateVertices(std::vector<GLfloat> &vertices);
private:
  GLuint indexBuffer = 0;
  GLuint vertexArray = 0;
  GLuint vertexBuffer = 0;
private:
  unsigned int indexBufferSize = 0;
};
