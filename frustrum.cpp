#include "include/frustrum.hpp"

Frustrum::Frustrum() {
  return;
}
Frustrum::~Frustrum() {
  return;
}
void Frustrum::update(glm::mat4 projection, glm::mat4 view) {
  float clip[4][4];
	clip[0][0] = view[0][0] * projection[0][0] + view[0][1] * projection[1][0] +
  view[0][2] * projection[2][0] + view[0][3] * projection[3][0];
  clip[0][1] = view[0][0] * projection[0][1] + view[0][1] * projection[1][1] +
  view[0][2] * projection[2][1] + view[0][3] * projection[3][1];
  clip[0][2] = view[0][0] * projection[0][2] + view[0][1] * projection[1][2] +
  view[0][2] * projection[2][2] + view[0][3] * projection[3][2];
  clip[0][3] = view[0][0] * projection[0][3] + view[0][1] * projection[1][3] +
  view[0][2] * projection[2][3] + view[0][3] * projection[3][3];

  clip[1][0] = view[1][0] * projection[0][0] + view[1][1] * projection[1][0] +
  view[1][2] * projection[2][0] + view[1][3] * projection[3][0];
  clip[1][1] = view[1][0] * projection[0][1] + view[1][1] * projection[1][1] +
  view[1][2] * projection[2][1] + view[1][3] * projection[3][1];
  clip[1][2] = view[1][0] * projection[0][2] + view[1][1] * projection[1][2] +
  view[1][2] * projection[2][2] + view[1][3] * projection[3][2];
  clip[1][3] = view[1][0] * projection[0][3] + view[1][1] * projection[1][3] +
  view[1][2] * projection[2][3] + view[1][3] * projection[3][3];

  clip[2][0] = view[2][0] * projection[0][0] + view[2][1] * projection[1][0] +
  view[2][2] * projection[2][0] + view[2][3] * projection[3][0];
  clip[2][1] = view[2][0] * projection[0][1] + view[2][1] * projection[1][1] +
  view[2][2] * projection[2][1] + view[2][3] * projection[3][1];
  clip[2][2] = view[2][0] * projection[0][2] + view[2][1] * projection[1][2] +
  view[2][2] * projection[2][2] + view[2][3] * projection[3][2];
  clip[2][3] = view[2][0] * projection[0][3] + view[2][1] * projection[1][3] +
  view[2][2] * projection[2][3] + view[2][3] * projection[3][3];

  clip[3][0] = view[3][0] * projection[0][0] + view[3][1] * projection[1][0] +
  view[3][2] * projection[2][0] + view[3][3] * projection[3][0];
  clip[3][1] = view[3][0] * projection[0][1] + view[3][1] * projection[1][1] +
  view[3][2] * projection[2][1] + view[3][3] * projection[3][1];
  clip[3][2] = view[3][0] * projection[0][2] + view[3][1] * projection[1][2] +
  view[3][2] * projection[2][2] + view[3][3] * projection[3][2];
  clip[3][3] = view[3][0] * projection[0][3] + view[3][1] * projection[1][3] +
  view[3][2] * projection[2][3] + view[3][3] * projection[3][3];

  data[RIGHT][A] = clip[0][3] - clip[0][0];
	data[RIGHT][B] = clip[1][3] - clip[1][0];
	data[RIGHT][C] = clip[2][3] - clip[2][0];
	data[RIGHT][D] = clip[3][3] - clip[3][0];
	normalize(RIGHT);

	data[LEFT][A] = clip[0][3] + clip[0][0];
	data[LEFT][B] = clip[1][3] + clip[1][0];
	data[LEFT][C] = clip[2][3] + clip[2][0];
	data[LEFT][D] = clip[3][3] + clip[3][0];
	normalize(LEFT);

	data[BOTTOM][A] = clip[0][3] + clip[0][1];
	data[BOTTOM][B] = clip[1][3] + clip[1][1];
	data[BOTTOM][C] = clip[2][3] + clip[2][1];
	data[BOTTOM][D] = clip[3][3] + clip[3][1];
	normalize(BOTTOM);

	data[TOP][A] = clip[0][3] - clip[0][1];
	data[TOP][B] = clip[1][3] - clip[1][1];
	data[TOP][C] = clip[2][3] - clip[2][1];
	data[TOP][D] = clip[3][3] - clip[3][1];
	normalize(TOP);

	data[FRONT][A] = clip[0][3] - clip[0][2];
	data[FRONT][B] = clip[1][3] - clip[1][2];
	data[FRONT][C] = clip[2][3] - clip[2][2];
	data[FRONT][D] = clip[3][3] - clip[3][2];
	normalize(FRONT);

	data[BACK][A] = clip[0][3] + clip[0][2];
	data[BACK][B] = clip[1][3] + clip[1][2];
	data[BACK][C] = clip[2][3] + clip[2][2];
	data[BACK][D] = clip[3][3] + clip[3][2];
	normalize(BACK);
}
bool Frustrum::isInside(std::pair<glm::vec3, glm::vec3> box) {
  auto getPlane = [](Frustrum *frustrum, Plane plane) {
      return glm::vec4(frustrum->data[plane][A], frustrum->data[plane][B],
      frustrum->data[plane][C], frustrum->data[plane][D]);
  };
  auto isInsidePlane = [](glm::vec4 clip, std::pair<glm::vec3, glm::vec3> box) {
		float x0 = box.first.x * clip.x;
		float x1 = box.second.x * clip.x;
		float y0 = box.first.y * clip.y;
		float y1 = box.second.y * clip.y;
		float z0 = box.first.z * clip.z + clip.w;
		float z1 = box.second.z * clip.z + clip.w;
		float p1 = x0 + y0 + z0;
		float p2 = x1 + y0 + z0;
		float p3 = x1 + y1 + z0;
		float p4 = x0 + y1 + z0;
		float p5 = x0 + y0 + z1;
		float p6 = x1 + y0 + z1;
		float p7 = x1 + y1 + z1;
		float p8 = x0 + y1 + z1;
    if(p1 <= 0 && p2 <= 0 && p3 <= 0 && p4 <= 0 && p5 <= 0 && p6 <= 0 && p7 <= 0 && p8 <= 0) return false;
    else return true;
	};
  return isInsidePlane(getPlane(this, RIGHT), box) &&
  isInsidePlane(getPlane(this, LEFT), box) &&
  isInsidePlane(getPlane(this, TOP), box) &&
  isInsidePlane(getPlane(this, BOTTOM), box) &&
  isInsidePlane(getPlane(this, FRONT), box);
}
bool Frustrum::isInside(glm::vec3 point) {
  for(int i = 0; i < 6; i += 1) {
    bool outside =
    data[i][A] * point.x +
    data[i][B] * point.y +
    data[i][C] * point.z +
    data[i][D] <= 0;
    if(outside) return false;
  }
  return true;
}
void Frustrum::normalize(Plane plane) {
  float magnitude = glm::sqrt(
    data[plane][A] * data[plane][A] +
    data[plane][B] * data[plane][B] +
    data[plane][C] * data[plane][C]
  );
  data[plane][A] /= magnitude;
  data[plane][B] /= magnitude;
  data[plane][C] /= magnitude;
  data[plane][D] /= magnitude;
}
