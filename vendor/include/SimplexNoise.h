/**
 * @file    SimplexNoise.h
 * @brief   A Perlin Simplex Noise C++ Implementation (1D, 2D, 3D).
 *
 * Copyright (c) 2014-2018 Sebastien Rombauts (sebastien.rombauts@gmail.com)
 *
 * Distributed under the MIT License (MIT) (See accompanying file LICENSE.txt
 * or copy at http://opensource.org/licenses/MIT)
 */
#pragma once

#include <cstddef>  // size_t

/**
 * @brief A Perlin Simplex Noise C++ Implementation (1D, 2D, 3D, 4D).
 */
class SimplexNoise {
public:
    // 1D Perlin simplex noise
    static float noise(float x);
    // 2D Perlin simplex noise
    static float noise(float x, float y);
    // 3D Perlin simplex noise
    static float noise(float x, float y, float z);

    // Fractal/Fractional Brownian Motion (fBm) noise summation
    static float fractal(size_t octaves, float x);
    static float fractal(size_t octaves, float x, float y);
    static float fractal(size_t octaves, float x, float y, float z);

    static void shufflePermutationTable();

    /**
     * Constructor of to initialize a fractal noise summation
     *
     * @param[in] frequency    Frequency ("width") of the first octave of noise (default to 1.0)
     * @param[in] amplitude    Amplitude ("height") of the first octave of noise (default to 1.0)
     * @param[in] lacunarity   Lacunarity specifies the frequency multiplier between successive octaves (default to 2.0).
     * @param[in] persistence  Persistence is the loss of amplitude between successive octaves (usually 1/lacunarity)
     */
    // explicit SimplexNoise(float frequency = 1.0f,
    //                       float amplitude = 1.0f,
    //                       float lacunarity = 2.0f,
    //                       float persistence = 0.5f) :
    //     mFrequency(frequency),
    //     mAmplitude(amplitude),
    //     mLacunarity(lacunarity),
    //     mPersistence(persistence) {
    // }

private:
    // Parameters of Fractional Brownian Motion (fBm) : sum of N "octaves" of noise
    static constexpr float mFrequency = 1.0f;   ///< Frequency ("width") of the first octave of noise (default to 1.0)
    static constexpr float mAmplitude = 1.0f;   ///< Amplitude ("height") of the first octave of noise (default to 1.0)
    static constexpr float mLacunarity = 2.1f;  ///< Lacunarity specifies the frequency multiplier between successive octaves (default to 2.0).
    static constexpr float mPersistence = 0.5f; ///< Persistence is the loss of amplitude between successive octaves (usually 1/lacunarity)
};
