#version 300 es
precision mediump float;
precision mediump int;
uniform sampler2D atlas;
uniform int cameraUnderwater;
out vec4 color;

in float fragmentFaceIndex;
in vec2 fragmentUV;
in float visibility;

const float ambientStrength = 1.1f;
const float diffuseStrength = 0.3f;

const vec3 lightDirection = vec3(0.2f, -1.0f, 0.2f);
vec4 skyColor = vec4(0.612, 0.753, 0.98, 1.0);
const vec3 lightColor = vec3(1.0f, 0.996f, 0.937f);
const vec3 ambientColor = lightColor * ambientStrength;
// Face normals have been manually verified...
const vec3 faceNormals[7] = vec3[7](
  vec3(0.0f, 0.0f, 1.0f),
  vec3(0.0f, 0.0f, -1.0f),
  vec3(0.0f, 1.0f, 0.0f),
  vec3(0.0f, -1.0f, 0.0f),
  vec3(-1.0f, 0.0f, 0.0f),
  vec3(1.0f, 0.0f, 0.0f),
  vec3(0.0f, 1.0f, 0.0f)
);

void main() {
  if(cameraUnderwater == 1) {
    skyColor = vec4(0.1, 0.415, 0.56, 1.0);
  }
  vec4 textureFragment = texture(atlas, fragmentUV).rgba;
  float alpha = textureFragment.a;
  if(fragmentFaceIndex == 6.0) alpha = 0.8;
  if(alpha < 0.7) discard;
  float diffuseFactor = max(dot(faceNormals[int(fragmentFaceIndex)], normalize(-1.0f * lightDirection)), 0.0) * diffuseStrength;
  vec3 diffuseColor = diffuseFactor * lightColor;
  color = vec4((ambientColor + diffuseColor) * textureFragment.rgb, alpha);
  color = mix(skyColor, color, visibility);
  if(cameraUnderwater == 1) color = color * vec4(0.07, 0.7, 0.94, 1.0);
}
