#version 300 es
layout(location = 0) in vec3 vertexPosition;
out vec3 fragmentPosition;
uniform mat4 rotationProjection;
void main() {
  fragmentPosition = vertexPosition;
  gl_Position = rotationProjection * vec4(vertexPosition, 1.0f);
}
