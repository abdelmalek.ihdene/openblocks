#version 300 es
precision mediump float;
precision mediump int;
layout(location = 0) in vec3 position;
layout(location = 1) in float vertexFaceIndex;
layout(location = 2) in vec2 vertexUV;

out float fragmentFaceIndex;
out vec2 fragmentUV;
out float visibility;

uniform mat4 view;
uniform mat4 viewProjection;
uniform float currentTime;
uniform int cameraUnderwater;

float fogDensity = 0.005;
float fogGradient = 5.0;

void main() {
  if(cameraUnderwater == 1) {
    fogDensity = 0.1;
    fogGradient = 1.0;
  }
  fragmentFaceIndex = vertexFaceIndex;
  fragmentUV = vertexUV;
  float distance = length(vec3(view * vec4(position, 1.0)));
  visibility = exp(-1.0f * pow(distance * fogDensity, fogGradient));
  visibility = clamp(visibility, 0.0, 1.0);
  if(vertexFaceIndex == 6.0f) {
    float yVal = position.y + min(0.15 * sin(position.x + currentTime / 1.8f) + 0.15 * sin(position.z + currentTime / 1.3f), 0.15) - 0.4;
    gl_Position = viewProjection * vec4(vec3(position.x, yVal, position.z), 1.0);
  }
  else
    gl_Position = viewProjection * vec4(position, 1.0);
}
