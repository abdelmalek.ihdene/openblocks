#version 300 es
precision mediump float;
in vec3 fragmentPosition;
uniform samplerCube cubeMap;
out vec4 color;
void main() {
  color = texture(cubeMap, fragmentPosition);
}
